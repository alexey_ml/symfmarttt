/* configure require.js */
requirejs.config({

    baseUrl: '/app/module',
    urlArgs: "bust=" + (new Date()).getTime(), // cache bust while developing

    paths: {
        'jquery': '/vendor/bootstrap3/bower_components/jquery/dist/jquery.min',
        'underscore': '/vendor/bootstrap3/bower_components/underscore/underscore-min',
        'backbone': '/vendor/bootstrap3/bower_components/backbone/backbone-min',
        'marionette': '/vendor/bootstrap3/bower_components/backbone.marionette/lib/backbone.marionette.min',
        'app': '/app/module/App/App'
    },

    shim: {
        'underscore': {
            exports: '_'
        },

        'backbone': {
            deps: [ 'jquery', 'underscore' ],
            exports: 'Backbone'
        },

        marionette: {
            deps: [ 'backbone' ],
            exports: 'Marionette'
        }

    },

    config: {
        'i18n/i18n': {
            locale: 'en'
        }
    }

});


requirejs(

    [ 'app', 'backbone' ], function ( App, Backbone ) {

        $(document).ready(function () {

            window.App = new App();
            window.App.start();
            Backbone.history.start();

        });

    }, function( err ) {
        /* Errors handling here while in development */
        console.log( err );
    }

);
