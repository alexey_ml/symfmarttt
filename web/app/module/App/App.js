define([

    'marionette', 'App/Router', 'App/TopMenuView', 'App/FooterView'

], function(

    Marionette, Router, TopMenuView, FooterView

) {

    return App = Marionette.Application.extend({

        region: '#main-content',

        LayoutViews: {},
        ItemViews: {},
        CollectionsViews: {},
        Models: {},
        Collections: {},
        Router: {},

        RequestFormat: {
            JSON: 'application/json',
            XML: 'application/xml'
        },

        SupportedLang: {
            EN: 'en'
        },

        AcceptedLang: {
           en: 'en;q=1'
        },

        /**
         * Setup global for App data
         * @returns {App}
         */
        setupEssential: function(){
            var tSelf = this;

            $( document ).ajaxSend(function( event, jqXHR ) {
                jqXHR.setRequestHeader('Accept-Language', tSelf.AcceptedLang['en']);
                jqXHR.setRequestHeader('Accept', tSelf.RequestFormat.JSON);
                jqXHR.setRequestHeader('X-Accept-Version', 'v1');

            });

            return this;
        },

        /**
         * Setup Common Components
         * @returns {App}
         */
        setupComponents: function() {

            this.ItemViews.topMenu = new TopMenuView();
            this.ItemViews.footer = new FooterView();

            this.Router = new Router();
            return this;
        },

        /**
         * Auto Hook
         */
        initialize: function() {
            this
                .setupEssential()
                .setupComponents();

        }

    });

});