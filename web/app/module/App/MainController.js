define([

    'marionette', 'App/MainLayoutView',
    'ImageGallery/AlbumsCollection', 'ImageGallery/AlbumsView',
    'ImageGallery/AlbumLayoutView', 'ImageGallery/AlbumModel', 'ImageGallery/AlbumItemView',
    'ImageGallery/AlbumPaginatorModel', 'ImageGallery/AlbumPaginatorView',
    'i18n/i18n!i18n/nls/messages'

], function(

    Marionette, MainLayoutView,
    AlbumsCollection, AlbumsView,
    AlbumLayoutView, AlbumModel, AlbumItemView,
    AlbumPaginatorModel, AlbumPaginatorView,
    Messages

) {

    return Marionette.Controller.extend({

        home: function() {

            console.log('Home Route Invoked');

            window.App.LayoutViews.main = new MainLayoutView();
            window.App.LayoutViews.main.render();
            window.App.LayoutViews.main.getRegion('menuRegion').show(window.App.ItemViews.topMenu, {});
            window.App.LayoutViews.main.getRegion('footerRegion').show(window.App.ItemViews.footer, {});

            window.App.Collections.albums = new AlbumsCollection();
            window.App.CollectionsViews.albums = new AlbumsView({ collection: window.App.Collections.albums  });

            window.App.CollectionsViews.albums.collection.fetch()

                .then(function(){

                    window.App.CollectionsViews.albums.render();
                    window.App.LayoutViews.main
                        .getRegion('mainRegion')
                        .show(window.App.CollectionsViews.albums, {});
                })

                .fail(function(){
                    alert(Messages.FetchOperationFailed); // Or redirect to err page
                });


        },

        album: function( id, page ) {

            console.log('Album Route Invoked');

            window.App.LayoutViews.album = new AlbumLayoutView();
            window.App.LayoutViews.album.render();
            window.App.LayoutViews.album.getRegion('menuRegion').show(window.App.ItemViews.topMenu, {});
            window.App.LayoutViews.album.getRegion('footerRegion').show(window.App.ItemViews.footer, {});

            window.App.Models.album = new AlbumModel();
            window.App.ItemViews.album = new AlbumItemView({ model: window.App.Models.album  });

            window.App.ItemViews.album.model.fetch({
                url: window.App.ItemViews.album.model.generateFetchUrl(id, page)
            })

                .then(function(){

                    window.App.ItemViews.album.render();
                    window.App.LayoutViews.album.getRegion('mainRegion').show(window.App.ItemViews.album, {});

                    if (window.App.ItemViews.album.model.get('total') > 10) {
                        console.log('Show Pagi');
                        window.App.Models.albumPaginator = new AlbumPaginatorModel({
                            'totalItems': window.App.ItemViews.album.model.get('total'),
                            'currentPage': page,
                            'itemsLimit': 10,
                            'albumId': id
                        });

                        window.App.ItemViews.albumPaginator = new AlbumPaginatorView({
                            model: window.App.Models.albumPaginator
                        });

                        window.App.LayoutViews.album
                            .getRegion('paginatorRegion')
                            .show(window.App.ItemViews.albumPaginator);

                    }

                })

                .fail(function(){
                    alert(Messages.FetchOperationFailed);
                });
        }

    });

});