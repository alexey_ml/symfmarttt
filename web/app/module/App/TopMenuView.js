define([

    'marionette'

], function(

    Marionette

) {

    return Marionette.ItemView.extend({

        template: "#top-menu-content",
        tagName: 'nav'

    });

});