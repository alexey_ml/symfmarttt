define([

    'marionette', 'App/MainController'

], function(

    Marionette, MainController

) {

    return Marionette.AppRouter.extend({

        controller: new MainController(),

        appRoutes: {

            '': 'home',
            'home': 'home',
            'api/album/:id': 'album',
            'api/album/:id/page/:page': 'album',
            '*notFound' : 'home' // Here it should be a 404 page

        }

    });

});