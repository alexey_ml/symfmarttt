define([

    'marionette'

], function(

    Marionette

) {

    return Marionette.ItemView.extend({

        template: "#footer-content",
        tagName: 'section'

    });

});