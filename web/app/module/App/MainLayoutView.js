define([

    'marionette'

], function(

    Marionette

) {

    return Marionette.LayoutView.extend({

        el: '#main-content',
        template: '#main-layout-view-template',

        regions: {
            menuRegion: '#menu',
            mainRegion: '#content',
            footerRegion: '#footer'
        }

    });

});