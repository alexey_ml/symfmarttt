define([

    'marionette'

], function(

    Marionette

) {

    return Marionette.ItemView.extend({

        tagName: 'table',
        template: '#album-view'

    });

});