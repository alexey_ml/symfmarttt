define([

    'backbone'

], function(

    Backbone

) {

    return Backbone.Model.extend({

        defaults: {
            id: '',
            name: '',
            total: 0,
            images: []
        },

        /**
         *
         * @param id
         * @param page
         * @returns {string}
         */
        generateFetchUrl: function(id, page){

            var url = '/api/album/' + parseInt(id);
            if (page) {
                url += '/page/' + parseInt(page);
            }

            return url;
        }

    });

});