define([

    'marionette'

], function(

    Marionette

) {

    return Marionette.LayoutView.extend({

        el: '#main-content',
        template: '#album-layout-view-template',

        regions: {
            menuRegion: '#menu',
            mainRegion: '#content',
            paginatorRegion: '#paginator',
            footerRegion: '#footer'
        }

    });

});