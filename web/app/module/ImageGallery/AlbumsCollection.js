define([

    'backbone', 'ImageGallery/AlbumsModel'

], function(

    Backbone, AlbumsModel

) {

    return Backbone.Collection.extend({

        model: AlbumsModel,
        url: '/api/albums'

    });

});