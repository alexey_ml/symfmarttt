define([

    'marionette'

], function(

    Marionette

) {

    return Marionette.ItemView.extend({

        tagName: 'div',
        template: '#album-paginator'

    });

});