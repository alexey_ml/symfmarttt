define([

    'backbone'

], function(

    Backbone

) {

    return Backbone.Model.extend({

        defaults: {
            totalItems: 0,
            currentPage: 1,
            itemsLimit: 10,
            albumId: 0
        }

    });

});