define([

    'marionette', 'ImageGallery/AlbumsItemView'

], function(

    Marionette, AlbumsItemView

) {

    return Marionette.CollectionView.extend({

        childView: AlbumsItemView,
        tagName: 'table'

    });

});