Symfony 2.8 / Backbone.Marionette test project
========
**
You can find a lot of information about this application in the [Issues Tracker](https://bitbucket.org/alexey_ml/symfmarttt/issues) and corresponding [Branches](https://bitbucket.org/alexey_ml/symfmarttt/branches/?status=merged). Use **master** or **Sprint1** branches to review the actual code.
Here is a brief instruction how to create a local copy of App and Summary.** 


# Setup the local instance of Application #

### 1. Prepare directories ###

I use **/home/symfmarttt/www** as a root for this application.

It is possible to create all the necessary stuff for such case with these commands for example (for dev environment):

```
#!bash

root@su# adduser --disabled-login --home /home/symfmarttt symfmarttt
root@su# usermod -a -G www-data symfmarttt
root@su# usermod -a -G symfmarttt www-data
root@su# cd /home/symfmarttt/
root@su:/home/symfmarttt# mkdir www
root@su:/home/symfmarttt# mkdir logs
```

Set the needed permissions depended on your OS



### 2. Clone the project and install vendors ###

Make a local copy of App's source with: 


```
#!bash

git clone https://alexey_ml@bitbucket.org/alexey_ml/symfmarttt.git .
```

and that install the vendors as they are in .gitignore:


```
#!bash

composer install
```



### 3. Prepare Database and load Fixtures ###

Create some database for this application, with something like


```
#!sql

CREATE DATABASE `symfmarttt` /*!40100 DEFAULT CHARACTER SET utf8 */
```

and after that, create the tables by the Entities or with these queries:


```
#!sql

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `album`;
CREATE TABLE `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `album_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `image_album_id_fk` (`album_id`),
  CONSTRAINT `image_album_id_fk` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;

```

Load the fixtures with: 


```
#!bash

php app/console doctrine:fixtures:load
```

There are some already existing fixtured images: **web/uploads/images** so no images uploading/adding needed


### 4. Add a configuration file ###

If needed, make a copy of app/config/parameters.yml.dist to app/config/parameters.yml and write in the database parameters, like


```
#!yaml

# This file is auto-generated during the composer install
parameters:
    database_host: 127.0.0.1
    database_port: null
    database_name: symfmarttt
    database_user: theuser
    database_password: thepassword
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    secret: XXXXXXthesecretXXXXXXX
```

 

### 5. Prepare Apache and nginx ###

Bring in the Apache vhost config with something like:


```
#!conf

<VirtualHost *:80>
        ServerName symfmarttt.local
        DocumentRoot /home/symfmarttt/www/public/

        # Override default apache denies
        <Directory />
            Options all
            AllowOverride all
            Require all granted
        </Directory>

        SetEnv APPLICATION_ENV development
        CustomLog /home/symfmarttt/logs/symfmarttt.access.log combined
        ErrorLog /home/symfmarttt/logs/symfmarttt.error.log
        ServerAdmin admin@symfmarttt.local
</VirtualHost>
```

and a possible example of nginx vhost:


```
#!conf

server {
  listen *:80;
  server_name symfmarttt.ukrone.net;
  client_max_body_size 200M;
  access_log /home/symfmarttt/logs/nginx.access.log;
  error_log /home/symfmarttt/logs/logs/nginx.error.log;
  open_file_cache_errors off;

  location / {
    proxy_pass http://localhost:8081;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
  }
  location ~* \.(jpg|jpeg|gif|png|ico|css|bmp|swf|js|txt)$ {
    root /home/symfmarttt/www/web/;
  }

}

```



### 6. Access via HTTP ###

Add something like 


```
#!conf

127.0.0.1	symfmarttt.local
```

to your **hosts** file (**/etc/hosts**)

so you were able to access the pages with 

**http://symfmarttt.local/app_dev.php** or **http://symfmarttt.local** URLs


### 7. Run Auto Tests ###

Run tests with:

```
#!bash

phpunit -c app/
```

something like this expected:


```
#!bash

PHPUnit 4.8.26 by Sebastian Bergmann and contributors.

........

Time: 1.28 seconds, Memory: 44.00MB

OK (8 tests, 11 assertions)

```



### 8. Live Demo ###

Check out the live demo by this URL:

http://symfmarttt.ukrone.net/

or a shoter one:

http://goo.gl/H71204


# Project Description and References #

### Backend ###

I use FOSRestBundle to provide API functionality

**
src/ImageGalleryBundle** - Image Gallery Main Bundle

**src/AppBundle/Entity**, **src/AppBundle/Repository** - Doctrine Entities and Repositories

**src/AppBundle/Controller/DefaultController.php** - Landing

**src/ImageGalleryBundle/Controller/AlbumApiController.php** - FOSRestBundle API Controller

### Frontend ###

I use **require.js / AMD** architecture to load JS/Marionette modules

**web/app/main.js** - main JS entry point, other files are loading with require.js

**web/app/module** - where all the JS files are located

**web/vendor** - vendors, it uses bower and already compiled bootstrap 3 theme, but its already in repo so no installation needed


### Fixtures ###

Fixtures are located at **src/AppBundle/DataFixtures/ORM**


### Tests ###

**Functional** and **Unit** tests are located at:

**src/AppBundle/Tests**

**src/ImageGalleryBundle/Tests**

## Notes ##

PHP Version of my dev machine: **PHP 5.5.9-1ubuntu4.16**

*- **Before you start**: check Issues tracker for additional information on this task, like commands, database struture, bundles and so on;
- Each task is done in its own branch, Task ID is **T-ID** in branches;
- Filter issues with **All** filter to see all the tasks done;
- Current version is **v.1.0** and current sprint is **Sprint1** and **branch Sprint1** thus
*
## Some words about possible improvements and ToDo ##

1. *As its require.js / AMD architecture, a lots of scripts be load initially. This can be optimized with r.js for examples in the real conditions.*

2. *Hardcoded URL in javascript (like /api/xxx/yy) - better to hold URLs in one place, like in the Routers (Symfony or Backbone one) and generate URL from there, so its a space to improvement for the real project.*

3. *Marionette.js and CoffeeScript. It was required to develop frontend with Marionette.js framework and style code with CoffeeScript. Actually, previously I dealt with pure Backbone.js / Underscore.js (with plain Application = {} and so on), so I had to learn marionette for a one-two days. It is backbone based for sure but it has a lot of extra stuff. The same situation is with CoffeScript, I never used it before (heard about tho), so I decided to not risk with CofeeScript and coded scripts on pure JavaScript. If it is a hard required I will remaster JSs with CoffeScript in the same repo during a couple of days. Hope it will not break down the rest of my job.*

4. *Static templates - there are not too much templates right now, so they are static and load with 'home page', without dynamic loading and something like TemplateManager or TemplateFactory with caching etc.*
5. *404, 500 errors - 404 cases are just redirecting to 'home' route, and error handler is just an alert() right now. Still can be improved.*

6. *Marionette optimization - render() is an expensive operation, I believe my code might be improved to not render some components on each Route event.*

7. *Get 10 images for Albums list - this was a space to think about. I could add one-to-many getter for Album Entity of use Album Repository which is better and load Images for each Album separetely (like 10 Albums, 10 SQL queries). Or I could make one query, select images for current Albums and process them in PHP. Both methods are worth, especially on some conditions, like caching mechanics and size of database, but I decided to use one query to get all the images at once for this moment.*