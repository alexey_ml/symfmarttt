<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ImageRepository
 */
class ImageRepository extends EntityRepository
{
    /**
     * @param array $albums
     * @return array
     */
    public function getImagesForAlbums($albums)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('img')
            ->from('AppBundle:Image', 'img')
            ->where('img.album IN (:albums)')
            ->setParameter('albums', $albums);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $album_id
     * @param int $page
     * @param \Knp\Component\Pager\PaginatorInterface $paginator
     * @return array
     */
    public function getImagesForAlbum($album_id, $page, $paginator)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('img.name')
            ->from('AppBundle:Image', 'img')
            ->where('img.album=:album_id')
            ->setParameter('album_id', $album_id);

        /* @var \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination $pRes */
        $pRes = $paginator->paginate($qb, $page, 10);

        return array_column($pRes->getItems(), 'name');
    }

    /**
     * @param int $album_id
     * @return int
     */
    public function getTotalImagesForAlbum($album_id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('COUNT(img)')
            ->from('AppBundle:Image', 'img')
            ->where('img.album=:album_id')
            ->setParameter('album_id', $album_id);

        return $qb->getQuery()->getSingleScalarResult();
    }

}