<?php

namespace AppBundle\Super;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;

abstract class SuperApiController extends FOSRestController
{
    const RESPONSE_OK_STAT = 200;
    const RESPONSE_DATA_ERR_STAT = 422; // 422 Unprocessable Entity
    const RESPONSE_SERVER_ERROR_STAT = 500;
    const RESPONSE_FORBIDDEN = 403;
    const RESPONSE_UNAUTHORIZED = 401;

    /**
     * @use $code HTTP Status Code
     * @param int $code
     * @param array $data
     * @param array $headers
     * @return Response
     */
    protected function getResponse($code, $data = array(), $headers = array())
    {
        /* @var \FOS\RestBundle\View\View $view */
        $view = $this->view($data, $code, $headers);
        return $this->handleView($view);
    }

}