<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/9/16
 * Time: 12:38 PM
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Album;
use AppBundle\Entity\Image;

class LoadImageGallery implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /**
         * Add Albums
         * Lets assume that $i is an id for the album
         */
        for ($i = 1; $i <= 5; $i++) {
            $album = new Album();
            $album->setName('Test Album ' . $i);

            $manager->persist($album);
            $manager->flush();

            /**
             * Add images to the albums
            * We have 5 pre-loaded images fixture(1..5).jpg
             */
            for ($j = 1; $j <= 20; $j++) {
                $image = new Image();
                $image->setName('fixture' . rand(1, 5) . '.jpg');
                $image->setAlbum($album);

                if ($i == 1 && $j == 6) {
                    break;
                }

                $manager->persist($image);
                $manager->flush();

            }

        }
    }
}