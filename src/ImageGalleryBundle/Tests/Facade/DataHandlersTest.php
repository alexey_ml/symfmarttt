<?php

namespace FeedbackBundle\Tests\Facade;

use ImageGalleryBundle\Facade\AlbumDataHandler;
use \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\ORM\EntityManager;
use \Symfony\Component\DependencyInjection\ContainerInterface;


class HandlersTest extends KernelTestCase
{
    /* @var ContainerInterface self::$container */
    protected static $container;

    /* @var EntityManager self::$entityManager */
    protected static $entityManager;

    /* @var AlbumDataHandler self::$albumDataHandler */
    protected static $albumDataHandler;


    /**
     * Actually here is nothing to mock so far
     * Lets ninja mock paginator and check if it works
     * Just to use mock-object
     */
    public function testNinjaMockPaginator()
    {
        /* @var \Knp\Component\Pager\Paginator|\PHPUnit_Framework_MockObject_MockObject| $ninjaMockPaginator */
        $ninjaMockPaginator = $this->getMock('\Knp\Component\Pager\Paginator');
        $ninjaMockPaginator
            ->expects($this->any())
            ->method('paginate')
            ->will($this->returnValue([['fixture1.jpg'], ['fixture2.jpg'], ['fixture3.gpg']]));

        $res = $ninjaMockPaginator->paginate(1, 2, 3);
        $this->assertEquals(3, count($res));

    }

    /**
     * Test AlbumDataHandler functionality on the fixtured record
     */
    public function testAlbumDataHandler()
    {
        $res = self::$albumDataHandler->getData(2, 1);
        $this->assertTrue(!empty($res));
        $this->assertEquals(20, $res['total']);
        $this->assertEquals(10, count($res['images']));
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function setUpBeforeClass()
    {
        /* @var \Symfony\Component\HttpKernel\KernelInterface $kernel */
        $kernel = static::createKernel();
        $kernel->boot();
        self::$container = $kernel->getContainer();
        self::$entityManager = self::$container->get('doctrine.orm.entity_manager');
        self::$albumDataHandler = self::$container->get('api.album_data_handler');


        /**
         * Re-Create Tables and load data
         * In real conditions we can recreate tables in the test database
         * and fill it with data from fixtures
         * This code is here as an example for test task
         */

        /*
        self::$entityManager->getConnection()->executeQuery("

            SET FOREIGN_KEY_CHECKS = 0;


            DROP TABLE IF EXISTS `album`;
            CREATE TABLE `album` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `name` varchar(50) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


            DROP TABLE IF EXISTS `image`;
            CREATE TABLE `image` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `name` varchar(50) NOT NULL,
              `album_id` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `image_album_id_fk` (`album_id`),
              CONSTRAINT `image_album_id_fk` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            SET FOREIGN_KEY_CHECKS = 1;

        ");
        */

    }

}