<?php

namespace ImageGalleryBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


use Symfony\Bundle\FrameworkBundle\Client;

class AlbumApiControllerTest extends WebTestCase
{
    /* @var Client static::$client */
    protected static $client;

    /**
     * Test for album list
     */
    public function testAlbumsList()
    {
        static::$client->request('GET', '/api/albums');
        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
    }

    /**
     * Test for the request for Album view without page param
     * Test that fixtured entities (in terms of the test task at least) work
     * Use test database with loaded fixtures in real situations
     */
    public function testAlbumWithoutPagination()
    {
        static::$client->request('GET', '/api/album/1');
        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
    }

    /**
     * Test for Album view with page param
     */
    public function testAlbumWithPagination()
    {
        static::$client->request('GET', '/api/album/2/page/1');
        $this->assertEquals(200, static::$client->getResponse()->getStatusCode());
    }

    /**
     * Test if non-digits id and page are not allowed
     */
    public function testWrongAndPageFormatForAlbumsApi()
    {
        static::$client->request('GET', '/api/album/x');
        $this->assertFalse(static::$client->getResponse()->isSuccessful());

        static::$client->request('GET', '/api/album/1/page/x');
        $this->assertFalse(static::$client->getResponse()->isSuccessful());
    }

    /**
     * Setup client once per this test suite with static hook
     */
    public static function setUpBeforeClass()
    {
        static::$client = static::createClient();
    }

}
