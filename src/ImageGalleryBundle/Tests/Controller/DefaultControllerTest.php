<?php

namespace ImageGalleryBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /**
     * Just dummy to keep the prepared controller's test
     */
    public function testNothing()
    {
        $this->assertTrue(true);

    }
}
