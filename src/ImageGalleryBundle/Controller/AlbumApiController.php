<?php

namespace ImageGalleryBundle\Controller;

use AppBundle\Super\SuperApiController;
use ImageGalleryBundle\Facade\AlbumDataHandler;
use ImageGalleryBundle\Facade\AlbumsDataHandler;
use Symfony\Component\HttpFoundation\Response;


class AlbumApiController extends SuperApiController
{

    /**
     * @param $id
     * @param $page
     * @return Response
     */
    public function albumAction($id, $page)
    {
        /* @var AlbumDataHandler $dataHandler */
        $dataHandler = $this->get('api.album_data_handler');

        return $this->getResponse(SuperApiController::RESPONSE_OK_STAT, $dataHandler->getData($id, $page));
    }

    /**
     * @return Response
     */
    public function albumsAction()
    {
        /* @var AlbumsDataHandler $dataHandler */
        $dataHandler = $this->get('api.albums_data_handler');

        return $this->getResponse(SuperApiController::RESPONSE_OK_STAT, $dataHandler->getData(10));

    }
}
