<?php

namespace ImageGalleryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DefaultController
 * @use Keep this class for non-api purposes, like form generating etc
 *
 * @package ImageGalleryBundle\Controller
 */

class DefaultController extends Controller
{
}