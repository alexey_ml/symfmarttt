<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/12/16
 * Time: 11:53 AM
 */

namespace ImageGalleryBundle\Formatter;


use AppBundle\Entity\Album;

class AlbumDataFormatter implements AlbumDataFormatterInterface
{
    /**
     * @param Album $album
     * @param array $images
     * @param int $total
     * @return array
     */
    public function formatData($album, $images, $total)
    {
        $res = new \ArrayObject();

        $res->offsetSet('id', $album->getId());
        $res->offsetSet('name', $album->getName());
        $res->offsetSet('total', $total);
        $res->offsetSet('images', $images);

        return (array) $res;
    }
}