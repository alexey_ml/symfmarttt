<?php

namespace ImageGalleryBundle\Formatter;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;

class AlbumsDataFormatter implements AlbumsDataFormatterInterface
{
    /**
     * @param \ArrayObject $albums
     * @param \ArrayObject $images
     * @param int $imgLimit
     * @return array
     */
    public function formatData($albums, $images, $imgLimit)
    {

        $result = [];

        foreach($albums AS $album) {

            /* @var Album $album */
            $albumId = $album->getId();

            $albObj = new \ArrayObject();
            $albObj->offsetSet('id', $albumId);
            $albObj->offsetSet('name', $album->getName());

            $i = 0;
            $imgs = [];

            foreach($images AS $image) {
                if ($i == $imgLimit) {
                    break;
                }

                /* @var Image $image */
                $_albId = $image->getAlbum()->getId();
                if ($albumId == $_albId) {
                    $imgs[] = $image->getName();
                    $i++;
                }
            }

            $albObj->offsetSet('images', $imgs);
            $result[] = $albObj;
        }

        return $result;
    }
}