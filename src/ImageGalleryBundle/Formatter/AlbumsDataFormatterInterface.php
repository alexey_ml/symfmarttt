<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/12/16
 * Time: 11:58 AM
 */

namespace ImageGalleryBundle\Formatter;


interface AlbumsDataFormatterInterface
{
    /**
     * @param \ArrayObject $albums
     * @param \ArrayObject $images
     * @param int $imgLimit
     * @return array
     */
    public function formatData($albums, $images, $imgLimit);
}