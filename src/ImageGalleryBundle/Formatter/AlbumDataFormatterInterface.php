<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/12/16
 * Time: 11:56 AM
 */

namespace ImageGalleryBundle\Formatter;


use AppBundle\Entity\Album;

interface AlbumDataFormatterInterface
{
    /**
     * @param Album $album
     * @param array $images
     * @param int $total
     * @return array
     */
    public function formatData($album, $images, $total);
}