<?php

namespace ImageGalleryBundle\Facade;


use AppBundle\Entity\Album;
use AppBundle\Repository\AlbumRepository;
use AppBundle\Repository\ImageRepository;
use Doctrine\ORM\EntityManager;
use ImageGalleryBundle\Formatter\AlbumDataFormatterInterface;
use \Knp\Component\Pager\PaginatorInterface;
use \Knp\Component\Pager\Paginator;

class AlbumDataHandler
{
    /* @var EntityManager $this->em */
    protected $em;

    /* @var PaginatorInterface|Paginator $this->paginator */
    protected $paginator;

    /* @var AlbumDataFormatterInterface $this->dataFormatter */
    protected $dataFormatter;

    /**
     * @param int $id
     * @param int $page
     * @return array
     */
    public function getData($id, $page)
    {
        /* @var AlbumRepository $albRepo */
        $albRepo = $this->em->getRepository('AppBundle:Album');

        /* @var Album $album*/
        $album = $albRepo->findOneBy(['id' => (int)$id]);

        if (empty($album)) {
            return [];
        }

        /* @var ImageRepository $imgRepo*/
        $imgRepo = $this->em->getRepository('AppBundle:Image');
        $images = $imgRepo->getImagesForAlbum($id, $page, $this->paginator);
        $totalImages = $imgRepo->getTotalImagesForAlbum($id);

        return $this->dataFormatter->formatData($album, $images, $totalImages);

    }


    /**
     * @param EntityManager $em
     * @return AlbumDataHandler
     */
    public function setEm($em)
    {
        $this->em = $em;
        return $this;
    }

    /**
     * @param Paginator|PaginatorInterface $paginator
     * @return AlbumDataHandler
     */
    public function setPaginator($paginator)
    {
        $this->paginator = $paginator;
        return $this;
    }

    /**
     * @param AlbumDataFormatterInterface $dataFormatter
     * @return AlbumDataHandler
     */
    public function setDataFormatter($dataFormatter)
    {
        $this->dataFormatter = $dataFormatter;
        return $this;
    }



}