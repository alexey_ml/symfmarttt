<?php

namespace ImageGalleryBundle\Facade;


use AppBundle\Repository\AlbumRepository;
use AppBundle\Repository\ImageRepository;
use Doctrine\ORM\EntityManager;
use ImageGalleryBundle\Formatter\AlbumsDataFormatter;
use ImageGalleryBundle\Formatter\AlbumsDataFormatterInterface;

class AlbumsDataHandler
{
    /* @var EntityManager $this->em */
    protected $em;

    /* @var AlbumsDataFormatterInterface $this->dataFormatter */
    protected $dataFormatter;

    /**
     * AlbumsDataHandler constructor.
     * @param EntityManager $em
     * @param AlbumsDataFormatter $dataFormatter
     */
    public function __construct($em, $dataFormatter)
    {
        $this->em = $em;
        $this->dataFormatter = $dataFormatter;
    }

    /**
     * @param int $imgLimit
     * @return array
     */
    public function getData($imgLimit)
    {
        /* @var AlbumRepository $albRepo */
        $albRepo = $this->em->getRepository('AppBundle:Album');
        $albums = $albRepo->findAll();

        if (empty($albums)) {
            return [];
        }

        /* @var ImageRepository $imgRepo*/
        $imgRepo = $this->em->getRepository('AppBundle:Image');
        $images = $imgRepo->getImagesForAlbums($albums);

        return $this->dataFormatter->formatData(
            new \ArrayObject($albums),
            new \ArrayObject($images),
            $imgLimit
        );

    }


}